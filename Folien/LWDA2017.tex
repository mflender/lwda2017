% Copyright (c) 2017 Malte Flender, Carsten Gips and Christian Carsten Sander
%
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal 
% in the Software without restriction, including without limitation the rights 
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
% copies of the Software, and to permit persons to whom the Software is 
% furnished to do so, subject to the following conditions:
%
% The above copyright notice and this permission notice shall be included in all 
% copies or substantial portions of the Software.
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.

\documentclass[10pt,a5paper]{beamer}

% packages
\usepackage{xcolor}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{multicol}
\usepackage{nameref}
\usepackage{subfigure} % Multiple gaphics in one figure
\usepackage{bibgerm}
\usepackage{xcolor}
\usepackage[ruled,vlined,linesnumbered]{algorithm2e}
\usepackage{tikz}

\setcounter{tocdepth}{4}

% bibref with numbers
\setbeamertemplate{bibliography item}{\insertbiblabel}

% package configuration
\DeclareGraphicsExtensions{.pdf,.png,.jpg}
\beamertemplatenavigationsymbolsempty
\setbeamertemplate{footline}[frame number]

% style
\usetheme{default}

\usecolortheme{rose}

% title page information
\author[Flender, Gips]{Malte Flender, Carsten Gips}

\institute[FH Bielefeld]{FH Bielefeld - Campus Minden,\\
Artilleriestraße 9, 32427 Minden, Germany}

\title[Sentiment analysis, Twitter]
{Sentiment analysis of a German Twitter-Corpus}

\subject{Sentiment analysis of a German Twitter-Corpus}

\date{12.09.2017}

\keywords{Twitter, Sentiment Analysis, Machine Learning,
Classification, German}

%\titlegraphic{
%	\includegraphics[width=3cm]{figures/fhbi_logo_kompakt_orange.jpg}
%}


\addtobeamertemplate{frametitle}{}{%
\begin{tikzpicture}[remember picture, overlay]

%\node[anchor=north east, xshift=-10pt, yshift=0pt, scale=0.3] at
%(current page.north east) 
%{\includegraphics[]{figures/fhbi_logo_kompakt_orange.jpg}};

\end{tikzpicture}}


\newcommand{\button}[1]{
	\vfill
	\hfill
	\beamerbutton{#1}
}

% Macro for centering extreme wide tables/figures
\makeatletter
\newcommand*{\Centerfloat}{%
  \parindent \z@
  \leftskip \z@ \@plus 1fil \@minus \textwidth
  \rightskip\leftskip
  \parfillskip \z@skip}
\makeatother

\newcommand{\blfootnote}[1]{%
  \begingroup
  \renewcommand\thefootnote{}\footnote{#1}%
  \addtocounter{footnote}{-1}%
  \endgroup
}

%------------------------------------------------------------------------------
% Macros for quotes:
%------------------------------------------------------------------------------

% A direct quote
% @par1: The quoted text
% @par2: The source where the text is from
% @par3: The page where the text is from
\newcommand*{\QuoteDirect}[3]{\QuoteM{\emph{#1}} \cite[#3]{#2}}

% A direct quote without page
% @par1: The quoted text
% @par2: The source where the text is from
\newcommand*{\QuoteDirectNoPage}[2]{\QuoteM{\emph{#1}} \cite{#2}}

% A indirect quote
% @par1: The source where the text is from
% @par2: The page where the text is from
\newcommand*{\QuoteIndirect}[2]{(vgl. \cite[#2]{#1})}

% A indirect quote without page
% @par1: The source where the text is from
\newcommand*{\QuoteIndirectNoPage}[1]{(vgl. \cite{#1})}

% A text with quotation marks
% @par1: The text you want to quote
% »text«
\newcommand*{\QuoteM}[1]{\frqq #1\flqq}

% A text with single quotation marks
% @par1: The text you want to quote
% ›text‹
\newcommand*{\QuoteMs}[1]{\frq #1\flq}

% To adjust some words to the flow
% @par1: the adjusted words
% [text]
\newcommand*{\AdjustWords}[1]{{\normalfont[#1]}}

% Displays a reference to the given object
% @par1: the lable of the thing you want to see
% (Siehe auch Abbildung 1.1 »Ein Bild« auf Seite 4)
\newcommand*{\SeeS}[1]
{(siehe auch \autoref{#1} \QuoteM{\nameref{#1}} auf \autopageref{#1})}

% Displays a reference to the given object
% @par1: the lable of the thing you want to see
% (Siehe auch Abbildung 1.1 »Ein Bild« auf Seite 4)
\newcommand*{\SeeB}[1]
{(Siehe auch \autoref{#1} \QuoteM{\nameref{#1}} auf \autopageref{#1})}

% Displays a reference to an equation
% @par1: the lable of the equation you want to see
% (Siehe auch Gleichung 1.1 in »Dummy Section« auf Seite 5)
\newcommand*{\SeeEq}[1]
{(siehe auch \autoref{#1} in \QuoteM{\nameref{#1}} auf \autopageref{#1})}

% The symbol for a elision
% Is used for more than missings one word or a sentence
% [...]
\newcommand*{\Elision}{{\normalfont[\dots]}}

% The symbol for a small elision
% Is used for only one missing word
% [..]
\newcommand*{\ElisionSmall}{{\normalfont[..]}}

% This is used if a book is cited at whole 
% passim means something like continuous
% text (vgl. [Aut99, passim]).
\newcommand*{\passim}{passim}

% To show the audience that there is something
% To display a wrong/importen part but not corrected in the quote
% text error [sic!] text
\newcommand*{\SIC}{{\normalfont[sic!]}}

% The text for a note from the author
% text, Anm. d. Autors
\newcommand*{\NoteFromAuthor}{{\normalfont\unskip , Anm. d. Autors}}

\setbeamertemplate{section in toc}[sections numbered]
\setbeamertemplate{subsection in toc}[subsections numbered]

\makeatletter
\newcommand*{\currentname}{\@currentlabelname}
\makeatother

\setcounter{tocdepth}{4}

% Brake long url in cite
\def\UrlBreaks{\do\/\do-}

%\AtBeginSection[]
%{
%  \begin{frame}<beamer>{Table of contents}
%   %\begin{multicols}{2}
%     \tableofcontents[
%     sectionstyle=show/shaded,
%     subsectionstyle=show/show/shaded,
%     subsubsectionstyle=show/show/show/shaded]
%   %\end{multicols}
%  \end{frame}
%}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 30 Min Zeit -> 24 -- 36 Folien
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\frame{
	\titlepage
}
\frame{
	\frametitle{Table of contents}
	%\begin{multicols}{2}
	\tableofcontents
	%\end{multicols}
}

%------------------------------------------------------------------------------
\section{Introduction}
\begin{frame}{\insertsection}

\begin{figure}
	\Centerfloat
	\includegraphics[scale=0.50]{figures/mindmap.pdf}
    %\caption{Diagram of the different preprocessing and classification steps}
    %\label{fig:preprocessingFlow}
\end{figure}

\end{frame}

\section{Related Work} %TODO Wegdamit
\begin{frame}{Thumbs Up?: Sentiment Classification\\ Using Machine Learning 
Techniques}{\insertsection}

\begin{itemize}
	\setlength\itemsep{10 pt}
	
	\item First Idea:
	\begin{itemize}
		\item Using two lists of words
		\item Positive and negative
		\item Works poorly
		\item Accuracy: 58~\% to 64~\%
		\item Random-choice baseline: 50~\%
	\end{itemize}
	
	\item Second idea:
	\begin{itemize}
		\item Machine learning methods
		\item Support Vector Machine (SVM)
		\item Naive Bayes (NB)
		\item Maximum Entropy (ME)
		\item Bag-of-word features
		\item Accuracy: up to 82.9~\%
	\end{itemize}
	\item On movie review with two classes
\end{itemize}

\blfootnote{\cite{pang2002thumbs}}
\end{frame}

\begin{frame}{A Sentimental Education: Sentiment Analysis\\ Using Subjectivity 
Summarization Based on Minimum Cuts}{\insertsection\ (ctn.)}
\blfootnote{\cite{pang2004sentimental}}

\begin{itemize}
	\setlength\itemsep{15 pt}
	\item Cascaded approach with movie review:
	\begin{itemize}
		\item First separated into subjective/objective
		\item Then into positive/negative
	\end{itemize}
	\item Also a \textbf{graph based classification} algorithm were used
	\item Accuracy: up to 86.4~\%
	\item Using two classes
\end{itemize}

\end{frame}

\begin{frame}{Twitter Sentiment Classification\\ using Distant Supervision}
{\insertsection\ (ctn.)}

\begin{itemize}
	\setlength\itemsep{15 pt}
	\item Analysis of English tweets into two classes 
	\begin{itemize}
		\item Using NB, ME and SVM
		\item Bag-of-word features
		\item Emoticons as noisy labels
	\end{itemize}
	\item Accuracy: up to 83.0~\%
\end{itemize}

\blfootnote{\cite{go2009distant}}
\end{frame}

\section{Corpus}
\begin{frame}{\insertsection}
\blfootnote{\cite{dailabor2012twittersentiment}}

\begin{itemize}
	\setlength\itemsep{10 pt}
	\item Provided by Narr et al.
	\item Containing 1800 tweets with three labels (positive, neutral, 
	negative)
	\item Assigned by three different Amazon Mechanical Turk workers
	\item On a subset of 958 tweets all three workers agreed on
	\item Leading to an inter-human-agreement of $958/1800=53.22~\%$
\end{itemize}

\end{frame}

\section{Approach}
\subsection{Preprocessing} 
\begin{frame}{\insertsubsection}{\insertsection}

\begin{figure}
	\Centerfloat
	\includegraphics[scale=0.49]{figures/preprocessing_flow_2.pdf}
\end{figure}

\end{frame}

\subsection{Feature Extraction} 
\begin{frame}{\insertsubsection}{\insertsection\ (ctn.)}

\begin{itemize}
	\setlength\itemsep{15 pt}
	\item Sparse feature vector
	\begin{itemize}
		\setlength\itemsep{5 pt}
		\item List of the $k$ most common $n$-grams is used as feature vector
		\item Binary encoding
		\item Uniform feature space 
		\item Consumes much memory
	\end{itemize}
	\item Dense feature vector
	\begin{itemize}
		\setlength\itemsep{5 pt}
		\item For every tweet the feature vector consists of all $n$-grams 
		contained in the individual tweet
		\item Needs little memory 
		\item Asymmetric feature space
	\end{itemize}
\end{itemize}
\end{frame}

\subsection{Classification} 
\begin{frame}{\insertsubsection}{\insertsection\ (ctn.)}

\begin{itemize}
	\setlength\itemsep{10 pt}
	\item Ten-fold \textbf{cross validation} is used
	\item Corpus is \textbf{shuffled}
	\item Four different classifiers are used:
	\begin{itemize}
		\item \texttt{sklearn.naive\_bayes.MultinomialNB}
		\item \texttt{sklearn.svm.LinearSVC}
		\item \texttt{nltk.DecisionTreeClassifier}
		\item \texttt{nltk.MaxentClassifier}
	\end{itemize}
	\item A ZeroR classifier is used as \textbf{baseline}
	\begin{description}
		\item[Neutral:] 74.92~\%
		\item[Positive:] 15.07~\%
		\item[Negative:] 10.01~\%
	\end{description}
\end{itemize}

\end{frame}

\section{Evaluation \& Results}

\begin{frame}{Preprocessing small influence,\\ feature extraction big 
influence}
{\insertsection}

\begin{figure}
	\Centerfloat
	\resizebox{0.99\textwidth}{!}{\input{Bar_Chart_1}}
\end{figure}

\end{frame}

\begin{frame}{Neutral overpowered}{\insertsection\ (ctn.)}

The overall best performance is achieved with the \textbf{Maximum Entropy} 
classifier, using the \textbf{dense} feature vector, \textbf{uni- and bi-gram} 
encoding and \textbf{no stopword filtering} with an accuracy of 84.51~\%.

\note{Neutral besonders gut, over fitting auf neutral}

\begin{figure}
	\Centerfloat
	\resizebox{0.99\textwidth}{!}{\input{Bar_Chart_2}}
\end{figure}

\end{frame}

\begin{frame}{Dense faster (and better accuracy)}{\insertsection\ (ctn.)}

\begin{figure}
	\Centerfloat
	\resizebox{0.99\textwidth}{!}{\input{Bar_Chart_3}}
\end{figure}

\end{frame}

\section{Discussion}
\begin{frame}{Sparse as expected}{\insertsection} %TODO wegdamit

\begin{itemize}
	\setlength\itemsep{15 pt}
	\item Sparse behavior match Go et al. \cite{go2009distant}:
	\begin{itemize}
		\item Best performance with the \textbf{SVM}
		\item Combination of $n$ to $m$-grams is better than only $n$-grams
		\item The use of \textbf{preprocessing} steps, like stemming and 
		stopword removal, does \textbf{not} seem to have much \textbf{effect}
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Dense vs. Sparse feature vector}{\insertsection\ (ctn.)}
\begin{itemize}
	\setlength\itemsep{15 pt}
	\item Dense: More realistic model
	\begin{itemize}
		\item Dense: \textbf{Utilizing every $n$-gram}
		\item Sparse: Only the \textbf{$k$ most common $n$-grams} are used
	\end{itemize}
	\item Asymmetric feature vectors are handled different by the classifiers
	\begin{itemize}
		\item Decision tree is created iteratively
		\item SVM skips all non-occurring features
	\end{itemize}
	\item Dense can \textbf{indirectly learn} about the term 
	\textbf{frequency}
	\begin{itemize}
		\item Dense: One word can occur \textbf{several times}
		\item Sparse: All words occur \textbf{only once}
	\end{itemize}	
\end{itemize}
\end{frame}


\begin{frame}{German vs. English}{\insertsection\ (ctn.)}

\textbf{Difference} between German and English (e.g. \cite{go2009distant}) is  
\textbf{small}.

\bigskip
\begin{itemize}
	\setlength\itemsep{15 pt}
	\item Stopwords, character encoding and the stemmer must be changed when 
	working with German text
	\item \textbf{Capitalization} has only a \textbf{small influence} on the 
	classification accuracy
\end{itemize}

\bigskip
Most ideas, which have been proven to work in English, should \textbf{easily} be \textbf{adaptable} to German.

\end{frame}

\begin{frame}{Ambiguousness of sentiment}{\insertsection\ (ctn.)} %TODO wegdamit

The ambiguousness of the sentiment classification task is shown by the 
inter-human-agreement.

\bigskip
\begin{itemize}
	\setlength\itemsep{15 pt}
	\item Only in 53.22~\% of the cases \textbf{all three humans} agreed on
	the sentiment
	\item In 95.50~\% of all given cases \textbf{two or more annotators} agreed 
	on the sentiment
\end{itemize}

\bigskip
Compared to our best result of 84.51~\% this displays how subjective sentiment 
classification even for real humans is.

\end{frame}

\begin{frame}{Approach vs. Related Work}{\insertsection\ (ctn.)}

\begin{itemize}
	\setlength\itemsep{15 pt}
	\item Our approach lies within the 
	\textbf{same range of accuracy} as Pang's et~al. and Go's et~al. approaches
	\begin{itemize}
		\item Two vs. three classes
		\item Movie reviews vs. tweets
	\end{itemize}
	\item Compared to Socher et al. our approach lacks about one percent 
	accuracy
	\begin{itemize}
		\item Two vs. three classes
		\item Movie reviews vs. tweets
		\item Treebank annotations on  word level vs. sentence level 
		annotations
	\end{itemize}
\end{itemize}

\blfootnote{\cite{pang2002thumbs, pang2004sentimental, go2009distant,
Socher-etal:2013}}
\end{frame}

\begin{frame}{Take home message}

While most of the \textbf{preprocessing} steps have only \textbf{small 
influence} on the final results the representation of the \textbf{feature 
vector} impacts the accuracy and the time performance \textbf{significantly}.

\bigskip
The difference between English and German is quite \textbf{small}.

\end{frame}

\section{Future Work} %TODO wegdamit
\begin{frame}{Improvements}{\insertsection}

\begin{itemize}
	\setlength\itemsep{15 pt}
	\item A larger corpus could be used to gain more reliable results
	\item Better features selecting method for the sparse vector
	\begin{itemize}
		\item Information gain
		\item $\chi^2$
	\end{itemize}
	\item Balancing all three cases to reduce the bias of the neutral case
	\item Using deep learning approaches
	\begin{itemize}
		\item Automated selection the features
		\item No bag-of-words approach, where structural information is lost
	\end{itemize}
\end{itemize}

\end{frame}

\begin{frame}[allowframebreaks]{Sources}
\footnotesize
\bibliographystyle{gerplain}
\bibliography{Literature}
\end{frame}

\frame[plain]{
	\Huge \center Any Questions?
}

%------------------------------------------------------------------------------

\end{document}
