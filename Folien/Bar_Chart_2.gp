# To make a .tex file: texlive-eepic and texlive-bxeepic needs to be installed
set terminal epslatex color size 16cm, 9cm

# The name an location of the .tex file
set output "Bar_Chart_2.tex"


set ylabel ""
set title ""

set key right top


fhOrange = "#ef8400"
fhBlue = "#1c95c2"
fhRed = "#b4007b"
fhGreen = "#a4b307"

set yrange [0:1]
set style data histogram
set style histogram cluster gap 1
set style fill solid
set boxwidth 0.9
set xtics format ""
set grid ytics

set offset -0.3,-0.3,0,0

plot "data_2.dat" using 2:xtic(1) title "Precision"   linecolor rgb fhOrange, \
             '' using 3         title "Recall"  linecolor rgb fhBlue, \
             '' using 4         title "F-measure"   linecolor rgb fhRed, \
