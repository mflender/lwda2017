## Sentiment analysis of a German Twitter-Corpus

Code, paper, poster and presentation for the LWDA 2017 @ Rostock

https://www.wiwi.uni-rostock.de/lwda2017/welcome/  
http://ceur-ws.org/Vol-1917/  
http://ceur-ws.org/Vol-1917/paper06.pdf  
