de_sentiment_agree3_dense generation: 0.566730976105

SVM dense: de_sentiment_agree3.tsv
Num Train: 854.1
Num Test: 94.9
Accuracy: 0.759697648376

Time used: 0.947268009186

positive precision: 0.666666666667
positive recall:    0.118547008547
positive F-measure: 0.193304002761

negative precision: 0.1
negative recall:    0.0125
negative F-measure: 0.0222222222222

neutral precision: 0.76100637904
neutral recall:    0.990247662785
neutral F-measure: 0.859786105307
––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

Naive dense: de_sentiment_agree3.tsv
Num Train: 854.1
Num Test: 94.9
Accuracy: 0.757614781635

Time used: 0.980851888657

positive precision: 0.525
positive recall:    0.0733012820513
positive F-measure: 0.126081871345

negative precision: 0.1
negative recall:    0.0125
negative F-measure: 0.0222222222222

neutral precision: 0.757390568708
neutral recall:    0.994157782516
neutral F-measure: 0.859116513247
––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

[Found megam: /usr/local/bin/megam]
[Found megam: /usr/local/bin/megam.opt]
MaxEnt dense: de_sentiment_agree3.tsv
Num Train: 854.1
Num Test: 94.9
Accuracy: 0.751276595745

Time used: 7.00142788887

positive precision: 0.4
positive recall:    0.0803311965812
positive F-measure: 0.12856032856

negative precision: 0.1
negative recall:    0.0125
negative F-measure: 0.0222222222222

neutral precision: 0.756275936103
neutral recall:    0.986155784514
neutral F-measure: 0.855098652387
––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

Tree dense: de_sentiment_agree3.tsv
Num Train: 854.1
Num Test: 94.9
Accuracy: 0.756539753639

Time used: 6595.93007684

positive precision: 0.466666666667
positive recall:    0.0741025641026
positive F-measure: 0.125540654997

negative precision: 0.2
negative recall:    0.0215909090909
negative F-measure: 0.0388888888889

neutral precision: 0.758822972774
neutral recall:    0.991723550082
neutral F-measure: 0.858959847421
––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

