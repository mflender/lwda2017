de_sentiment_agree3_sparse generation: 12.4863951206
[Found megam: /usr/local/bin/megam]
[Found megam: /usr/local/bin/megam.opt]
MaxEnt sparse: de_sentiment_agree3.tsv
Num Train: 854.1
Num Test: 94.9
Accuracy: 0.769171332587

Time used: 1021.06709599

positive precision: 0.552369281046
positive recall:    0.394861111111
positive F-measure: 0.433928556213

negative precision: 0.0666666666667
negative recall:    0.025
negative F-measure: 0.0363636363636

neutral precision: 0.792689268888
neutral recall:    0.95644197932
neutral F-measure: 0.865231823684
––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

SVM sparse: de_sentiment_agree3.tsv
Num Train: 854.1
Num Test: 94.9
Accuracy: 0.831321388578

Time used: 123.742959976

positive precision: 0.728347902098
positive recall:    0.674829059829
positive F-measure: 0.692873114365

negative precision: 0.526428571429
negative recall:    0.218831168831
negative F-measure: 0.294888444888

neutral precision: 0.858534857947
neutral recall:    0.946479759335
neutral F-measure: 0.89987047373
––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

Tree sparse: de_sentiment_agree3.tsv
Num Train: 854.1
Num Test: 94.9
Accuracy: 0.825061590146

Time used: 11987.9752061

positive precision: 0.765303030303
positive recall:    0.633023504274
positive F-measure: 0.688598303341

negative precision: 0.455952380952
negative recall:    0.207788600289
negative F-measure: 0.276446886447

neutral precision: 0.851982295289
neutral recall:    0.950738314969
neutral F-measure: 0.898111499448
––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

Naive sparse: de_sentiment_agree3.tsv
Num Train: 854.1
Num Test: 94.9
Accuracy: 0.475285554311

Time used: 121.32228899

positive precision: 0.326102836178
positive recall:    0.868450854701
positive F-measure: 0.46714661273

negative precision: 0.201636779013
negative recall:    0.572276334776
negative F-measure: 0.291750312305

neutral precision: 0.90021169039
neutral recall:    0.384745415915
neutral F-measure: 0.537200737095
––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

