de_sentiment_agree3_sparse generation: 4.10326886177
Naive sparse: de_sentiment_agree3.tsv
Num Train: 854.1
Num Test: 94.9
Accuracy: 0.702821948488

Time used: 21.0278589725

positive precision: 0.342712287712
positive recall:    0.314262820513
positive F-measure: 0.318285730219

negative precision: 0.21
negative recall:    0.075202020202
negative F-measure: 0.106593406593

neutral precision: 0.781877426669
neutral recall:    0.866673027384
neutral F-measure: 0.821044278497
––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

Tree sparse: de_sentiment_agree3.tsv
Num Train: 854.1
Num Test: 94.9
Accuracy: 0.775543113102

Time used: 2661.53582597

positive precision: 0.763571428571
positive recall:    0.228098290598
positive F-measure: 0.327411803882

negative precision: 0.35
negative recall:    0.0515909090909
negative F-measure: 0.0856421356421

neutral precision: 0.783038106268
neutral recall:    0.983589482116
neutral F-measure: 0.870916630765
––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

SVM sparse: de_sentiment_agree3.tsv
Num Train: 854.1
Num Test: 94.9
Accuracy: 0.773404255319

Time used: 21.1899678707

positive precision: 0.58323953824
positive recall:    0.314177350427
positive F-measure: 0.396271121668

negative precision: 0.506666666667
negative recall:    0.112070707071
negative F-measure: 0.173538961039

neutral precision: 0.798951586013
neutral recall:    0.95569164288
neutral F-measure: 0.869364108471
––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

[Found megam: /usr/local/bin/megam]
[Found megam: /usr/local/bin/megam.opt]
MaxEnt sparse: de_sentiment_agree3.tsv
Num Train: 854.1
Num Test: 94.9
Accuracy: 0.758600223964

Time used: 155.278096914

positive precision: 0.561666666667
positive recall:    0.171655982906
positive F-measure: 0.244727436306

negative precision: 0.2
negative recall:    0.0316666666667
negative F-measure: 0.0525

neutral precision: 0.769100097896
neutral recall:    0.977829903171
neutral F-measure: 0.85980126391
––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

