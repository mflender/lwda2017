de_sentiment_agree3_sparse generation: 4.84451699257
Tree sparse: de_sentiment_agree3.tsv
Num Train: 854.1
Num Test: 94.9
Accuracy: 0.771332586786

Time used: 3938.47600579

positive precision: 0.699484126984
positive recall:    0.238098290598
positive F-measure: 0.328384992201

negative precision: 0.533333333333
negative recall:    0.0940151515152
negative F-measure: 0.142712842713

neutral precision: 0.786626577622
neutral recall:    0.971337008222
neutral F-measure: 0.867968633072
––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

[Found megam: /usr/local/bin/megam]
[Found megam: /usr/local/bin/megam.opt]
MaxEnt sparse: de_sentiment_agree3.tsv
Num Train: 854.1
Num Test: 94.9
Accuracy: 0.754456886898

Time used: 240.890632868

positive precision: 0.541666666667
positive recall:    0.160384615385
positive F-measure: 0.235074729192

negative precision: 0.114285714286
negative recall:    0.0353968253968
negative F-measure: 0.0513888888889

neutral precision: 0.773178590278
neutral recall:    0.970645039041
neutral F-measure: 0.859646415968
––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

SVM sparse: de_sentiment_agree3.tsv
Num Train: 854.1
Num Test: 94.9
Accuracy: 0.786069428891

Time used: 31.4899377823

positive precision: 0.598968253968
positive recall:    0.346923076923
positive F-measure: 0.429823713962

negative precision: 0.483333333333
negative recall:    0.133585858586
negative F-measure: 0.199957542458

neutral precision: 0.813291203533
neutral recall:    0.963912484156
neutral F-measure: 0.881367332322
––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

Naive sparse: de_sentiment_agree3.tsv
Num Train: 854.1
Num Test: 94.9
Accuracy: 0.641713325868

Time used: 31.0426778793

positive precision: 0.272252638429
positive recall:    0.431581196581
positive F-measure: 0.3288043789

negative precision: 0.368430735931
negative recall:    0.243629148629
negative F-measure: 0.283006535948

neutral precision: 0.802952876836
neutral recall:    0.735015508322
neutral F-measure: 0.766368588041
––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

