de_sentiment_agree3_dense generation: 0.599009990692

Tree dense: de_sentiment_agree3.tsv
Num Train: 854.1
Num Test: 94.9
Accuracy: 0.827166853303

Time used: 8686.90895605

positive precision: 0.768815628816
positive recall:    0.640021367521
positive F-measure: 0.692656454167

negative precision: 0.454761904762
negative recall:    0.21687950938
negative F-measure: 0.284934281405

neutral precision: 0.852995530017
neutral recall:    0.950677928496
neutral F-measure: 0.898718330706
––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

SVM dense: de_sentiment_agree3.tsv
Num Train: 854.1
Num Test: 94.9
Accuracy: 0.830279955207

Time used: 1.15761709213

positive precision: 0.756644744145
positive recall:    0.677382478632
positive F-measure: 0.702524359796

negative precision: 0.561428571429
negative recall:    0.2031998557
negative F-measure: 0.288818958819

neutral precision: 0.853966627667
neutral recall:    0.948122005977
neutral F-measure: 0.897973951029
––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

Naive dense: de_sentiment_agree3.tsv
Num Train: 854.1
Num Test: 94.9
Accuracy: 0.826069428891

Time used: 1.25710391998

positive precision: 0.79196969697
positive recall:    0.594081196581
positive F-measure: 0.667176011424

negative precision: 0.55
negative recall:    0.125101010101
negative F-measure: 0.195959481695

neutral precision: 0.834824388721
neutral recall:    0.969080428008
neutral F-measure: 0.896341934117
––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

[Found megam: /usr/local/bin/megam]
[Found megam: /usr/local/bin/megam.opt]
MaxEnt dense: de_sentiment_agree3.tsv
Num Train: 854.1
Num Test: 94.9
Accuracy: 0.833426651736

Time used: 16.2121069431

positive precision: 0.782246576952
positive recall:    0.645363247863
positive F-measure: 0.694753390253

negative precision: 0.621666666667
negative recall:    0.200692640693
negative F-measure: 0.293542013542

neutral precision: 0.84641890772
neutral recall:    0.9592899002
neutral F-measure: 0.898620948829
––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

