de_sentiment_agree3_sparse generation: 1.55063796043
Naive sparse: de_sentiment_agree3.tsv
Num Train: 854.1
Num Test: 94.9
Accuracy: 0.716494960806

Time used: 10.1936981678

positive precision: 0.05
positive recall:    0.0105555555556
positive F-measure: 0.0172161172161

negative precision: 0.0
negative recall:    0.0
negative F-measure: 0.0

neutral precision: 0.743899711115
neutral recall:    0.95294177576
neutral F-measure: 0.834811236223
––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

Tree sparse: de_sentiment_agree3.tsv
Num Train: 854.1
Num Test: 94.9
Accuracy: 0.751287793953

Time used: 1397.89980602

positive precision: 0.2
positive recall:    0.0118055555556
positive F-measure: 0.0222910216718

negative precision: 0.0
negative recall:    0.0
negative F-measure: 0.0

neutral precision: 0.750676829342
neutral recall:    1.0
neutral F-measure: 0.856786174518
––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

SVM sparse: de_sentiment_agree3.tsv
Num Train: 854.1
Num Test: 94.9
Accuracy: 0.751287793953

Time used: 10.3487050533

positive precision: 0.2
positive recall:    0.0118055555556
positive F-measure: 0.0222910216718

negative precision: 0.0
negative recall:    0.0
negative F-measure: 0.0

neutral precision: 0.750676829342
neutral recall:    1.0
neutral F-measure: 0.856786174518
––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

[Found megam: /usr/local/bin/megam]
[Found megam: /usr/local/bin/megam.opt]
MaxEnt sparse: de_sentiment_agree3.tsv
Num Train: 854.1
Num Test: 94.9
Accuracy: 0.65758118701

Time used: 75.1956231594

positive precision: 0.162827978181
positive recall:    0.138739316239
positive F-measure: 0.120068560095

negative precision: 0.0
negative recall:    0.0
negative F-measure: 0.0

neutral precision: 0.746083173892
neutral recall:    0.850071741835
neutral F-measure: 0.789843880488
––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

