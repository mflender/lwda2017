de_sentiment_agree3_sparse generation: 3.35244894028
Naive sparse: de_sentiment_agree3.tsv
Num Train: 854.1
Num Test: 94.9
Accuracy: 0.756562150056

Time used: 11.5744838715

positive precision: 0.497857142857
positive recall:    0.186132478632
positive F-measure: 0.259420289855

negative precision: 0.15
negative recall:    0.0211111111111
negative F-measure: 0.0363636363636

neutral precision: 0.768334716337
neutral recall:    0.9732511205
neutral F-measure: 0.857678492579
––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

Tree sparse: de_sentiment_agree3.tsv
Num Train: 854.1
Num Test: 94.9
Accuracy: 0.77764837626

Time used: 1351.49897003

positive precision: 0.793333333333
positive recall:    0.190074786325
positive F-measure: 0.29532076111

negative precision: 0.35
negative recall:    0.0592929292929
negative F-measure: 0.100466200466

neutral precision: 0.775422181985
neutral recall:    0.994704570792
neutral F-measure: 0.870539474666
––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

SVM sparse: de_sentiment_agree3.tsv
Num Train: 854.1
Num Test: 94.9
Accuracy: 0.780806270997

Time used: 11.6884520054

positive precision: 0.693333333333
positive recall:    0.223878205128
positive F-measure: 0.333207669523

negative precision: 0.375
negative recall:    0.092702020202
negative F-measure: 0.143859473859

neutral precision: 0.783946163828
neutral recall:    0.98734911206
neutral F-measure: 0.873318551238
––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

[Found megam: /usr/local/bin/megam]
[Found megam: /usr/local/bin/megam.opt]
MaxEnt sparse: de_sentiment_agree3.tsv
Num Train: 854.1
Num Test: 94.9
Accuracy: 0.719697648376

Time used: 76.5027580261

positive precision: 0.353877788878
positive recall:    0.242126068376
positive F-measure: 0.262778466508

negative precision: 0.233333333333
negative recall:    0.0411111111111
negative F-measure: 0.0681818181818

neutral precision: 0.770916221417
neutral recall:    0.912801332527
neutral F-measure: 0.83369303477
––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

