de_sentiment_agree3_dense generation: 0.656366109848

SVM dense: de_sentiment_agree3.tsv
Num Train: 854.1
Num Test: 94.9
Accuracy: 0.752340425532

Time used: 0.999998092651

positive precision: 0.3
positive recall:    0.0194978632479
positive F-measure: 0.0365767359575

negative precision: 0.0
negative recall:    0.0
negative F-measure: 0.0

neutral precision: 0.751427109297
neutral recall:    1.0
neutral F-measure: 0.8572999388
––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

Naive dense: de_sentiment_agree3.tsv
Num Train: 854.1
Num Test: 94.9
Accuracy: 0.751287793953

Time used: 1.03529310226

positive precision: 0.2
positive recall:    0.0118055555556
positive F-measure: 0.0222910216718

negative precision: 0.0
negative recall:    0.0
negative F-measure: 0.0

neutral precision: 0.750676829342
neutral recall:    1.0
neutral F-measure: 0.856786174518
––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

[Found megam: /usr/local/bin/megam]
[Found megam: /usr/local/bin/megam.opt]
MaxEnt dense: de_sentiment_agree3.tsv
Num Train: 854.1
Num Test: 94.9
Accuracy: 0.749182530795

Time used: 7.48788404465

positive precision: 0.133333333333
positive recall:    0.0118055555556
positive F-measure: 0.0212885154062

negative precision: 0.0
negative recall:    0.0
negative F-measure: 0.0

neutral precision: 0.750029280776
neutral recall:    0.99696969697
neutral F-measure: 0.855298832746
––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

Tree dense: de_sentiment_agree3.tsv
Num Train: 854.1
Num Test: 94.9
Accuracy: 0.750223964166

Time used: 6281.74102902

positive precision: 0.1
positive recall:    0.00769230769231
positive F-measure: 0.0142857142857

negative precision: 0.0
negative recall:    0.0
negative F-measure: 0.0

neutral precision: 0.749921612542
neutral recall:    1.0
neutral F-measure: 0.856267330104
––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

