de_sentiment_agree3_sparse generation: 15.8108828068
de_sentiment_agree3_dense generation: 0.711067199707

SVM dense: de_sentiment_agree3.tsv
Num Train: 854.1
Num Test: 94.9
Accuracy: 0.830279955207

Time used: 5.0322740078

positive precision: 0.764551282051
positive recall:    0.647361111111
positive F-measure: 0.68934904543

negative precision: 0.563333333333
negative recall:    0.175137085137
negative F-measure: 0.256129426129

neutral precision: 0.842265709904
neutral recall:    0.962318683191
neutral F-measure: 0.89756629626
––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

SVM sparse: de_sentiment_agree3.tsv
Num Train: 854.1
Num Test: 94.9
Accuracy: 0.82712206047

Time used: 98.0838670731

positive precision: 0.759032356532
positive recall:    0.62125
positive F-measure: 0.675486369993

negative precision: 0.488333333333
negative recall:    0.175137085137
negative F-measure: 0.249640914641

neutral precision: 0.840894477304
neutral recall:    0.960722887683
neutral F-measure: 0.896244032263
––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

