real    0m7.254s                                                                                                              
user    0m7.160s                                                                                                              
sys     0m0.107s

MaxEnt dense: de_sentiment_agree3.tsv
Num Train: 854
Num Test: 95
Accuracy: 0.663157894737

positive precision: 0.318181818182
positive recall: 0.5
positive F-measure: 0.388888888889

negative precision: 0.428571428571
negative recall: 0.230769230769
negative F-measure: 0.3

neutral precision: 0.80303030303
neutral recall: 0.779411764706
neutral F-measure: 0.791044776119

   3.170 contains((u'lunett',))==True and label is u'neutral'
   3.170 contains((u'hung',))==True and label is u'neutral'
   3.170 contains((u'gestellt',))==True and label is u'neutral'
  -2.481 contains((u's',))==True and label is u'negative'
  -2.259 contains((u'"',))==True and label is u'negative'
  -2.013 contains((u'lieb',))==True and label is u'negative'
   1.995 contains((u'glaub', u'glaub'))==True and label is u'neutral'
  -1.973 contains((u'nintendo',))==True and label is u'positive'
  -1.949 contains((u'neu',))==True and label is u'negative'
  -1.778 contains((u'user',))==True and label is u'negative'

––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

