real    0m15.050s
user    0m14.868s
sys     0m0.120s

MaxEnt dense: de_sentiment_agree3.tsv
Num Train: 854
Num Test: 95
Accuracy: 0.631578947368

positive precision: 0.260869565217
positive recall: 0.428571428571
positive F-measure: 0.324324324324

negative precision: 0.428571428571
negative recall: 0.230769230769
negative F-measure: 0.3

neutral precision: 0.784615384615
neutral recall: 0.75
neutral F-measure: 0.766917293233

   4.248 contains((u'lunett',))==True and label is u'neutral'
   4.248 contains((u'hung',))==True and label is u'neutral'
   4.248 contains((u'gestellt',))==True and label is u'neutral'
   2.976 contains((u'glaub', u'glaub'))==True and label is u'neutral'
  -2.679 contains((u's',))==True and label is u'negative'
   2.586 contains((u'folg', u'kopf'))==True and label is u'neutral'
  -2.504 contains((u'"',))==True and label is u'negative'
  -2.338 contains((u'nintendo',))==True and label is u'positive'
  -2.318 contains((u'lieb',))==True and label is u'negative'
   2.188 contains((u'hallo', u'welt'))==True and label is u'neutral'

––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

