# coding=utf-8
 
# Copyright (c) 2017 Malte Flender, Carsten Gips and Christian Carsten Sander
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, merge, 
# publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons 
# to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.

import nltk 
import sys
import collections
import gc
import io
import random

from nltk.corpus import BracketParseCorpusReader
from nltk.corpus import stopwords
from nltk.classify.scikitlearn import SklearnClassifier
from multiprocessing import Pool
from sklearn import svm

# @ the moment not in use.
# from nltk import word_tokenize
# from sklearn import cross_validation


# TODO:
#    Maybe use the http://www.nltk.org/api/nltk.tokenize.html#module-nltk.tokenize.casual
#    Cross-Validation.
#    Make better use of the BracketParseCorpusReader.
#    Print the stats as LaTeX table.
#    Use information gain or case sensitivity to improve feature selection.
#    Use another max ent implementation maybe weka via the loader.

# The Path to the corpus files and the filenames.
# de_sentiment_small is only for testing.
corpus_root_path = "/data1/temporaer/WS2016⁄2017/Intelligente_Systeme/Projekt/Korpora/SentimentDataset/Data/"
file_pattern_names = ["de_sentiment.tsv", "de_sentiment_agree2.tsv", "de_sentiment_agree3.tsv"]

# To cope with the German umlauts.
reload(sys)
sys.setdefaultencoding("utf8")

stpwrds = stopwords.words("german") # Use the German stopwords.
# We added some additional stopwords.
stpwrds.extend([',', '.', '(', ')', '!', ';', '-', ':', '...',
                "''", "'", "``", '~http', '?', '@', '#', '/',
                '&', '--', '[', ']', '=', '||', '+', '_', '..',
                '%', '|', '~', 'http'])

# Use the German version of the snowball stemmer.
stemmer = nltk.stem.SnowballStemmer("german")

SEED = 4711  # The seed for the used in the shuffle prior to the classification.
NUM_MOST_COMMON_WORDS = 5000  # The number of the most common n-grams to be used.

# Generate from min to max-grams, e.g. uni- and bi-grams.
MIN_N_GRAM = 1 # The starting point of the n-gram generation, e.g. 1 -> unigram.
MAX_N_GRAM = 2 # The end point of the n-gram generation, e.g. 2 -> bigram.

MAXENT_MAX_ITER = 1 # The max number of iterations the max entropy classifier performs.

TRAIN_TEST_RATIO = 0.9 # The ratio between the amount of train and test data.

# Open the file with the given ID and return it.
def open_corpus(file_id):
    reader = BracketParseCorpusReader(corpus_root_path, file_pattern_names)
    file_content = reader.raw(fileids=reader.fileids()[file_id]).splitlines()
    return file_content

# Builds the sparse feature vector for a single tweet from the tweet, the sentiment and all words from the corpus.
# The tweet contains of a list where the first part is the list of n-grams and
#  the second or last part is the given sentiment.
# The returning vector contains mostly n-grams that are not in the tweet so most of it will be False.
def feature_extractor_sparse(tweet, all_words_from_the_corpus):
    sentiment = tweet[-1]
    dictinary = {}
    for word in all_words_from_the_corpus:
        dictinary["({})".format(word)] = (word in tweet[0])
    return (dictinary,) + (sentiment,)

# Builds the dense feature vector for a single tweet from the tweet and the sentiment.
# The returning vector contains only the n-grams from the given tweet.
def feature_extractor_dense(n_grams, sentiment):
    dictinary = {}
    for n_gram in n_grams:
        dictinary.update({"({})".format(n_gram) : True})
    return (dictinary,) + (sentiment,)

# Generate the from min_len to max_len grams, e.g. uni- and bi-grams.
def everygrams(sequence, min_len=1, max_len=-1):
    if max_len == -1:
        max_len = len(sequence)
    for n in range(min_len, max_len + 1):
        for ng in nltk.ngrams(sequence, n):
            yield ng

# Creates the train and test set from the corpus.
def generate_sets_sparse(file_id):
    features = []
    all_words = []
    tweets = []
    tweet_ID_last = ""

    for line in open_corpus(file_id):
        ignore = False
        parts_of_the_table = line.split("\t")
        
        if len(parts_of_the_table) == 4: # some lines from the corpus are broken and lead to a fourth class. So only perform on working lines.
            sentiment = parts_of_the_table[0] # split into columns.
            tweet_ID = parts_of_the_table[2] # save the sentiment.
            
            if (tweet_ID == tweet_ID_last) and (tweets[-1][-1] == sentiment):
                ignore = True # In the first set the sentiments are duplicate, so remove them.
                                # In the other files it woun'd change a bit.
            tweet_ID_last = tweet_ID
            
            if (sentiment != "na") and not ignore:
                tokens = nltk.tokenize.WordPunctTokenizer().tokenize(parts_of_the_table[-1].lower()) # Tokenize the feature and set it to lowercase.
                # tokens = nltk.word_tokenize(parts_of_the_table[-1].lower()) # Alternative tokenizer worser performance.
                small_tokens = [i for i in tokens if i not in stpwrds] # Remove all stopwords and additional stopwords.
                stem_tokens = [stemmer.stem(i) for i in small_tokens] # Stemming the feature optional step.
                n_grams = everygrams(stem_tokens, MIN_N_GRAM, MAX_N_GRAM) # Generate the n-grams.
                
                tmp_tweet_words = []
                for n_gram in n_grams:
                    tmp_tweet_words.append(n_gram)
                    if n_gram not in all_words:
                        all_words.append(n_gram) # Append to the list with all words.

                tweets.append([tmp_tweet_words, sentiment]) # Generate a list of the feature and the sentiment.
            
    f_dist = list(nltk.FreqDist(all_words))[:NUM_MOST_COMMON_WORDS] # Use only the k most common n-grams.
    tweets.pop(0) # Delete the first line since it contains the header.
    
    for tweet in tweets: # Generate the feature vector for every feature.
        for n_gram in tweet[0]:
            if n_gram not in f_dist: # Shrink the soon to be feature vector to the k most common n-grams.
                tweet[0].remove(n_gram)
        feature = feature_extractor_sparse(tweet, f_dist)
        features.append(feature)

    return features

# Creates the train and test set from the corpus.
def generate_sets_dense(file_id):
    features = []
    tweet_ID_last = ""
    
    for line in open_corpus(file_id):
        ignore = False
        parts_of_the_table = line.split("\t")
        if len(parts_of_the_table) == 4:
            sentiment = parts_of_the_table[0]
            tweet_ID = parts_of_the_table[2]
            
            if (tweet_ID == tweet_ID_last) and (features[-1][-1] == sentiment):
                ignore = True
            
            tweet_ID_last = tweet_ID
            
            if (sentiment != "na") and not ignore:
                tokens = nltk.tokenize.WordPunctTokenizer().tokenize(parts_of_the_table[-1].lower())
                small_tokens = [i for i in tokens if i not in stpwrds]
                stem_tokens = [stemmer.stem(i) for i in small_tokens]
                n_grams = everygrams(stem_tokens, MIN_N_GRAM, MAX_N_GRAM)
                feature = feature_extractor_dense(n_grams, sentiment)
                features.append(feature)
            
    features.pop(0)
    return features

# Return some statisitics from the classification
def get_stats(train_set, test_set, classifier):
    ref_set = collections.defaultdict(set)
    test_set_new = collections.defaultdict(set)
 
    for i, (feature_vector, label) in enumerate(test_set):
        ref_set[label].add(i)
        observed = classifier.classify(feature_vector)
        test_set_new[observed].add(i)
#         if observed != label:
#             print(str(observed) + ":\t" + str(label))
    
    out = "Num Train: " + str(len(train_set)) + "\n"
    out += "Num Test: " + str(len(test_set)) + "\n"
    out += "Accuracy: " + str(nltk.classify.accuracy(classifier, test_set)) + "\n"
    out += "\n"
    out += "positive precision: " + str(nltk.metrics.precision(ref_set["positive"], test_set_new["positive"])) + "\n"
    out += "positive recall: " + str(nltk.metrics.recall(ref_set["positive"], test_set_new["positive"])) + "\n"
    out += "positive F-measure: " + str(nltk.metrics.f_measure(ref_set["positive"], test_set_new["positive"])) + "\n"
    out += "\n"
    out += "negative precision: " + str(nltk.metrics.precision(ref_set["negative"], test_set_new["negative"])) + "\n"
    out += "negative recall: " + str(nltk.metrics.recall(ref_set["negative"], test_set_new["negative"])) + "\n"
    out += "negative F-measure: " + str(nltk.metrics.f_measure(ref_set["negative"], test_set_new["negative"])) + "\n"
    out += "\n"
    out += "neutral precision: " + str(nltk.metrics.precision(ref_set["neutral"], test_set_new["neutral"])) + "\n"
    out += "neutral recall: " + str(nltk.metrics.recall(ref_set["neutral"], test_set_new["neutral"])) + "\n"
    out += "neutral F-measure: " + str(nltk.metrics.f_measure(ref_set["neutral"], test_set_new["neutral"])) + "\n"
    out += "\n"
    return out
    
# Use a NaiveBayesClassifier to classify the features.
def classify_naive(content):
    
    # To use the map function only one argument can be passed, so it needs to be splitted.
    corpus = content[0]
    txt = content[1]
    
    # Some ideas for cross validation,
    #  but the get_stats must be changed in order to make it work . . .
#     cv = cross_validation.KFold(len(corpus), n_folds=10, shuffle=True)
#     for traincv, testcv in cv:
#         classifier = nltk.NaiveBayesClassifier.train(corpus[traincv[0]:traincv[len(traincv)-1]])
#         print 'accuracy:', nltk.classify.util.accuracy(classifier, corpus[testcv[0]:testcv[len(testcv)-1]])
    
    random.seed(SEED)
    random.shuffle(corpus)
    
    length = int(len(corpus) * TRAIN_TEST_RATIO)
    
    train_set = corpus[:length]
    test_set = corpus[length:]
    
    classifier = nltk.NaiveBayesClassifier.train(train_set)
    
    out = txt + "\n"
    out += get_stats(train_set, test_set, classifier)
    
    # In order to get show_most_informative_features to print in a a string and not to stdout
    #  stdout needs to be redirected.
    # If you skip this and use multicore 
    #  the prints will come out of sync and you don't know what belongs where.
    old_stdout = sys.stdout
    sys.stdout = mystdout = io.StringIO()
    classifier.show_most_informative_features(10)
    sys.stdout = old_stdout
    
    out += mystdout.getvalue() + "\n"
    out += "––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n"
    
    print(out)
    return classifier

# Use a DecisionTreeClassifier to classify the features.
def classify_tree(content):
    corpus = content[0]
    txt = content[1]
    
    random.seed(SEED)
    random.shuffle(corpus)
    
    length = int(len(corpus) * TRAIN_TEST_RATIO)
    
    train_set = corpus[:length]
    test_set = corpus[length:]
    
    classifier = nltk.DecisionTreeClassifier.train(train_set)
    
    out = txt + "\n"
    out += get_stats(train_set, test_set, classifier)
    out += classifier.pseudocode(depth=4)
    out += "––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n"
    
    print(out)
    return classifier

# Use a MaxentClassifier to classify the features.
def classify_maxent(content):
    corpus = content[0]
    txt = content[1]
    
    random.seed(SEED)
    random.shuffle(corpus)
    
    length = int(len(corpus) * TRAIN_TEST_RATIO)
    
    train_set = corpus[:length]
    test_set = corpus[length:]
    
    classifier = nltk.MaxentClassifier.train(train_set, max_iter=MAXENT_MAX_ITER, trace=0)
    # http://www.nltk.org/_modules/nltk/classify/maxent.html
    
    out = txt + "\n"
    out += get_stats(train_set, test_set, classifier)
    
    old_stdout = sys.stdout
    sys.stdout = mystdout = io.StringIO()
    classifier.show_most_informative_features(10)
    sys.stdout = old_stdout
    
    out += mystdout.getvalue() + "\n"
    out += "––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n"
    
    print(out)
    return classifier


# Use a SVM to classify the features.
def classify_svm(content):
    corpus = content[0]
    txt = content[1]
    
    random.seed(SEED)
    random.shuffle(corpus)
    
    length = int(len(corpus) * TRAIN_TEST_RATIO)
    
    train_set = corpus[:length]
    test_set = corpus[length:]

    # http://scikit-learn.org/stable/modules/classes.html#module-sklearn.svm
    # http://scikit-learn.org/stable/modules/generated/sklearn.svm.LinearSVC.html
    classifier = SklearnClassifier(svm.LinearSVC(
        C=1.0,
        class_weight=None,
        dual=True,
        fit_intercept=True,
        intercept_scaling=1,
        loss='squared_hinge',
        max_iter=1000,
        multi_class='ovr',
        penalty='l2',
        random_state=None,
        tol=1e-4,
        verbose=0))

    classifier.train(train_set)
    
    out = txt + "\n"
    out += get_stats(train_set, test_set, classifier)
    out += "––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n"
    
    print(out)
    return classifier

# In order to test the distribution after the shuffle this 
#  function counts and prints the occurrences for every feature in both set.
def classify_randtest(content):
    corpus = content[0]
    txt = content[1]
    
    neut_train = 0
    pos_train = 0
    neg_train = 0
    
    neut_test = 0
    pos_test = 0
    neg_test = 0
    
    # random.seed(SEED)
    random.shuffle(corpus)
    
    length = int(len(corpus) * TRAIN_TEST_RATIO)
    
    train_set = corpus[:length]
    test_set = corpus[length:]
    
    for i in train_set:
        if i[1] == "neutral":
            neut_train += 1
        elif i[1] == "positive":
            pos_train += 1
        elif i[1] == "negative":
            neg_train += 1
        else:
            print("ERROR: classify_randtest.train_set")
            return
            
    for i in test_set:
        if i[1] == "neutral":
            neut_test += 1
        elif i[1] == "positive":
            pos_test += 1
        elif i[1] == "negative":
            neg_test += 1
        else:
            print("ERROR: classify_randtest.test_set")
            return
            
    out = txt + "\n"
    out += ("neut_train: " + str(neut_train) + "\n")
    out += (" pos_train: " + str(pos_train) + "\n")
    out += (" neg_train: " + str(neg_train) + "\n")
    out += "\n"
    out += ("neut_test: " + str(neut_test) + "\n")
    out += (" pos_test: " + str(pos_test) + "\n")
    out += (" neg_test: " + str(neg_test) + "\n") 
    out += "––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n" 
    print(out)

# In order to get the baseline performance for the other classifiers
#  this function computes a sort of ZeroR-classification for all sentiments.
# The result depends on the shuffle.
def classify_ZeroR(content):
    corpus = content[0]
    txt = content[1]
    
    neutral = 0
    positive = 0
    negative = 0
    
    random.seed(SEED)
    random.shuffle(corpus)
    
    length = int(len(corpus) * TRAIN_TEST_RATIO)

    test_set = corpus[length:]
    
    for i in test_set:
        if i[1] == "neutral":
            neutral += 1
        elif i[1] == "positive":
            positive += 1
        elif i[1] == "negative":
            negative += 1
        else:
            print("ERROR: classify_ZeroR")
            return
 
    ac_neu = float(neutral) / len(test_set)
    ac_pos = float(positive) / len(test_set)
    ac_neg = float(negative) / len(test_set)
 
    out = txt + "\n"
    out += "Accuracy positive: " + str(ac_pos) + "\n"
    out += "Accuracy neutral:  " + str(ac_neu) + "\n"
    out += "Accuracy negative: " + str(ac_neg) + "\n"
    out += "––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––\n"
    print(out)

# Get the sparse and dense features and perform the classification with multicore support.
# To save some RAM one could perform the sparse and dense features in two separate runs.

de_sentiment_sparse = generate_sets_sparse(0)
#de_sentiment_dense = generate_sets_dense(0)
 
de_sentiment_agree2_sparse = generate_sets_sparse(1)
#de_sentiment_agree2_dense = generate_sets_dense(1)
 
de_sentiment_agree3_sparse = generate_sets_sparse(2)
#de_sentiment_agree3_dense = generate_sets_dense(2)

gc.collect()  # Trigger the garbage collector to free a little RAM.

pool = Pool(8)  # The number of logical cores.

#pool.map(classify_naive, [
    #(de_sentiment_sparse, "Naive sparse: de_sentiment.tsv",),
    #(de_sentiment_agree2_sparse, "Naive sparse: de_sentiment_agree2.tsv",),
    #(de_sentiment_agree3_sparse, "Naive sparse: de_sentiment_agree3.tsv",),
    #(de_sentiment_dense, "Naive dense: de_sentiment.tsv",),
    #(de_sentiment_agree2_dense, "Naive dense: de_sentiment_agree2.tsv",),
#    (de_sentiment_agree3_dense, "Naive dense: de_sentiment_agree3.tsv",)
#    ])
#gc.collect()

#pool.map(classify_svm, [
    #(de_sentiment_sparse, "SVM sparse: de_sentiment.tsv",),
    #(de_sentiment_agree2_sparse, "SVM sparse: de_sentiment_agree2.tsv",),
    #(de_sentiment_agree3_sparse, "SVM sparse: de_sentiment_agree3.tsv",),
    #(de_sentiment_dense, "SVM dense: de_sentiment.tsv",),
    #(de_sentiment_agree2_dense, "SVM dense: de_sentiment_agree2.tsv",),
#    (de_sentiment_agree3_dense, "SVM dense: de_sentiment_agree3.tsv",)
#    ])
#gc.collect()

pool.map(classify_maxent, [
    (de_sentiment_sparse, "MaxEnt sparse: de_sentiment.tsv",),
    (de_sentiment_agree2_sparse, "MaxEnt sparse: de_sentiment_agree2.tsv",),
    (de_sentiment_agree3_sparse, "MaxEnt sparse: de_sentiment_agree3.tsv",),
    #(de_sentiment_dense, "MaxEnt dense: de_sentiment.tsv",),
    #(de_sentiment_agree2_dense, "MaxEnt dense: de_sentiment_agree2.tsv",),
    #(de_sentiment_agree3_dense, "MaxEnt dense: de_sentiment_agree3.tsv",)
    ])
#gc.collect()

#pool.map(classify_tree, [
#    #(de_sentiment_sparse, "Tree sparse: de_sentiment.tsv",),
#    #(de_sentiment_agree2_sparse, "Tree sparse: de_sentiment_agree2.tsv",),
#    #(de_sentiment_agree3_sparse, "Tree sparse: de_sentiment_agree3.tsv",),
#    #(de_sentiment_dense, "Tree dense: de_sentiment.tsv",),
#    #(de_sentiment_agree2_dense, "Tree dense: de_sentiment_agree2.tsv",),
#    (de_sentiment_agree3_dense, "Tree dense: de_sentiment_agree3.tsv",)
#    ])

pool.close()
pool.join()
