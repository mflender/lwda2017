# To make a .tex file: texlive-eepic and texlive-bxeepic needs to be installed
set terminal epslatex color size 16cm, 9cm

# The name an location of the .tex file
set output "Bar_Chart_1.tex"


set ylabel "\\tiny Accuracy in percent"

set key right top


fhOrange = "#ef8400"
fhBlue = "#1c95c2"
fhRed = "#b4007b"
fhGreen = "#a4b307"

set yrange [70:85]
set style data histogram
set style histogram cluster gap 1
set style fill solid
set boxwidth 0.9
set xtics format ""
set grid ytics

set format y '\tiny %g'

set offset -0.3,-0.3,0,0

plot "data_1.dat" using 2:xtic(1) title "\\tiny NB"   linecolor rgb fhOrange, \
             '' using 3           title "\\tiny SVM"  linecolor rgb fhBlue, \
             '' using 4           title "\\tiny ME"   linecolor rgb fhRed, \
             '' using 5           title "\\tiny Tree" linecolor rgb fhGreen
